#!/bin/sh


AVRDUDE_VERSION="7.0"
AVRDUDE_FILE="avrdude-${AVRDUDE_VERSION}.tar.gz"
AVRDUDE_URL="http://download.savannah.gnu.org/releases/avrdude/${AVRDUDE_FILE}"


# Install packages
apt update
apt install bison flex libftdi-dev libusb-dev -y

# Get avrdude archive
wget -q $AVRDUDE_URL

# Unpack the avrdude archive
tar xf $AVRDUDE_FILE

# Compile the avrdude archive
cd avrdude-$AVRDUDE_VERSION
./configure --build=aarch64-linux-gnu --enable-linuxgpio --enable-linuxspi
make
make install

# Patch config file
cd ..
patch -d/usr/local/etc < avrdude/avrdude.conf.diff

# Remove avrdude directory
rm -rf avrdude-$AVRDUDE_VERSION
