#!/bin/sh


ARDUINO_VERSION="1.8.19"
ARDUINO_ARCH="aarch64"
ARDUINO_FILE="arduino-${ARDUINO_VERSION}-linux${ARDUINO_ARCH}.tar.xz"
ARDUINO_URL="https://downloads.arduino.cc/${ARDUINO_FILE}"


# Install packages
apt update
apt install wget xz-utils -y

# Get Arduino archive
if [ ! -f $ARDUINO_FILE ]; then
  wget -q $ARDUINO_URL
fi

# Unpack Arduino archive
tar xf $ARDUINO_FILE

# Copy necessary files
mkdir -p /usr/local/include/avr
cp -r arduino-$ARDUINO_VERSION/hardware/tools/avr/bin/avr-* /usr/local/bin
cp -r arduino-$ARDUINO_VERSION/hardware/tools/avr/etc/* /usr/local/etc
cp -r arduino-$ARDUINO_VERSION/hardware/tools/avr/lib/* /usr/local/lib
cp -r arduino-$ARDUINO_VERSION/hardware/tools/avr/libexec /usr/local
cp -r arduino-$ARDUINO_VERSION/hardware/tools/avr/aarch64-unknown-linux-gnu /usr/local
cp -r arduino-$ARDUINO_VERSION/hardware/tools/avr/include/* /usr/local/include/avr
cp -r arduino-$ARDUINO_VERSION/hardware/tools/avr/avr/bin/* /usr/local/libexec/gcc
cp -r arduino-$ARDUINO_VERSION/hardware/tools/avr/avr/lib/* /usr/local/lib
cp -r arduino-$ARDUINO_VERSION/hardware/tools/avr/avr/include/* /usr/local/include/avr

# Remove Arduino directory
rm -rf arduino-$ARDUINO_VERSION
