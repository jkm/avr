#include <avr/interrupt.h>

#define ADC_DELAY	10
#define INT_DELAY	65535


volatile uint8_t col;
volatile uint16_t adc;
volatile uint32_t pb;


ISR(USART0_TXC_vect) {
	USART0_STATUS |= USART_TXCIF_bm;
}

ISR(PORTA_PORT_vect) {
	if (PORTA_INTFLAGS & PIN6_bm) {
		col = 0;

		PORTA_INTFLAGS &= PIN6_bm;

		_delay_ms(ADC_DELAY);

		ADC0_COMMAND = ADC_STCONV_bm;
		while (!(ADC0_INTFLAGS & ADC_RESRDY_bm));
		adc = ADC0_RES;

		while (!(PORTA_IN & PIN6_bm) && pb < INT_DELAY)
			pb++;
	}
}

ISR(PORTC_PORT_vect) {
	if (PORTC_INTFLAGS & PIN2_bm) {
		col = 3;

		PORTC_INTFLAGS &= PIN2_bm;

		_delay_ms(ADC_DELAY);

		ADC0_COMMAND = ADC_STCONV_bm;
		while (!(ADC0_INTFLAGS & ADC_RESRDY_bm));
		adc = ADC0_RES;

		while (!(PORTC_IN & PIN2_bm) && pb < INT_DELAY)
			pb++;
	}
}

ISR(PORTB_PORT_vect) {
	if (PORTB_INTFLAGS & PIN2_bm) {
		col = 2;

		PORTB_INTFLAGS &= PIN2_bm;

		_delay_ms(ADC_DELAY);

		ADC0_COMMAND = ADC_STCONV_bm;
		while (!(ADC0_INTFLAGS & ADC_RESRDY_bm));
		adc = ADC0_RES;

		while (!(PORTB_IN & PIN2_bm) && pb < INT_DELAY)
			pb++;
	}

	if (PORTB_INTFLAGS & PIN6_bm) {
		col = 1;

		PORTB_INTFLAGS &= PIN6_bm;

		_delay_ms(ADC_DELAY);

		ADC0_COMMAND = ADC_STCONV_bm;
		while (!(ADC0_INTFLAGS & ADC_RESRDY_bm));
		adc = ADC0_RES;

		while (!(PORTB_IN & PIN6_bm) && pb < INT_DELAY)
			pb++;
	}
}
