#include <avr/io.h>


char KEYS[5][4] = {
	{ 'A', 'B', 'C', 'C' },
	{ '7', '8', '9', '/' },
	{ '4', '5', '6', 'x' },
	{ '1', '2', '3', '-' },
	{ '0', '.', '=', '+' }
};

void keypad_init() {
	PORTA_DIRCLR = PIN6_bm;
	PORTC_DIRCLR = PIN2_bm;
	PORTB_DIRCLR = (PIN2_bm | PIN6_bm);
	PORTA_PIN6CTRL |= PORT_PULLUPEN_bm | PORT_ISC_FALLING_gc;
	PORTC_PIN2CTRL |= PORT_PULLUPEN_bm | PORT_ISC_FALLING_gc;
	PORTB_PIN2CTRL |= PORT_PULLUPEN_bm | PORT_ISC_FALLING_gc;
	PORTB_PIN6CTRL |= PORT_PULLUPEN_bm | PORT_ISC_FALLING_gc;
}

uint8_t keypad_get_row(uint16_t adc) {
	if (adc > 3800)
		return 0;

	if (adc > 2300)
		return 1;

	if (adc > 1300)
		return 2;

	if (adc > 300)
		return 3;

	return 4;
}

char keypad_get_char(uint8_t col, uint16_t adc) {
	return KEYS[keypad_get_row(adc)][col];
}
