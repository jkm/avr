#include <avr/io.h>
#include <util/delay.h>

#include "spi.h"
#include "utils.h"

#define BUFFER_LEN	14


void lcd_on() {
	PORTC_OUTSET = PIN4_bm;
}

void lcd_off() {
	PORTC_OUTCLR = PIN4_bm;
}

void lcd_cmd(uint8_t cmd) {
	PORTA_OUTCLR = PIN4_bm;
	spi_tx(0x1f); // RS = 0, R/W = 0
	spi_tx(cmd & 0x0f);
	spi_tx((cmd >> 4) & 0x0f);
	PORTA_OUTSET = PIN4_bm;
}

void lcd_data(uint8_t data) {
	PORTA_OUTCLR = PIN4_bm;
	spi_tx(0x5f); // RS = 1, R/W = 0
	spi_tx(data & 0x0f);
	spi_tx((data >> 4) & 0x0f);
	PORTA_OUTSET = PIN4_bm;
}

void lcd_reset() {
	PORTC_OUTCLR = PIN5_bm;
	_delay_ms(200);
	PORTC_OUTSET = PIN5_bm;
	_delay_ms(100);
}

void lcd_init() {
	lcd_cmd(0x3a);
	lcd_cmd(0x09);
	lcd_cmd(0x06);
	lcd_cmd(0x1e);
	lcd_cmd(0x39);
	lcd_cmd(0x1b);
	lcd_cmd(0x6c);
	lcd_cmd(0x56);
	lcd_cmd(0x70);
	lcd_cmd(0x38);
	lcd_cmd(0x0f);
}

void lcd_clear() {
	lcd_cmd(0x01);
}

void lcd_set_rom(uint8_t rom) {
	lcd_cmd(0x3a);
	lcd_cmd(0x72);
	lcd_data(rom);
	lcd_cmd(0x38);
}

void lcd_write_char(uint8_t i, char ch) {
	lcd_cmd(i);
	lcd_data(ch);
}

void lcd_write_num(uint8_t addr, uint32_t num) {
	char buffer[BUFFER_LEN];

	for(uint8_t i = 0; i < int2chr(num, buffer); i++)
		lcd_write_char(addr | i, buffer[i]);
}
