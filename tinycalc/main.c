#include <avr/io.h>
#include <util/delay.h>

#include "lcd.h"
#include "adc.h"
#include "spi.h"
#include "isr.h"
#include "keypad.h"


int main() {
	adc_init();
	spi_init();
	keypad_init();
	// usart_init();

	// LCD POWER and RST
	PORTC_DIRSET = (PIN5_bm | PIN4_bm);
	PORTC_OUTSET = PIN5_bm;

	_delay_ms(200);

	// LCD POWER ON
	lcd_on();
	_delay_ms(50);
	lcd_reset();
	lcd_init();
	lcd_clear();

	sei();

	_delay_ms(200);

	for(;;) {
		if (pb > 3000 && pb < 30000) {
			lcd_clear();

			lcd_write_char(0x80, keypad_get_char(col, adc));
			lcd_write_num(0x80 | 0x20, col);
			lcd_write_num(0x80 | 0x40, adc);
			lcd_write_num(0x80 | 0x60, pb);

			adc = 0;
			pb = 0;
		}

		if (pb > 30000) {
			_delay_ms(200);
			lcd_reset();
			lcd_init();
			lcd_clear();

			adc = 0;
			pb = 0;
		}

		_delay_ms(100);
	}
}
