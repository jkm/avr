#include <avr/io.h>
#include <stdlib.h>


void spi_init() {
	PORTA_DIRSET = (PIN4_bm | PIN3_bm | PIN1_bm);
	PORTA_OUTSET = PIN4_bm;
	SPI0_CTRLA = SPI_DORD_bm | SPI_MASTER_bm | SPI_PRESC_DIV16_gc;
	SPI0_CTRLB = SPI_SSD_bm | SPI_MODE0_bm;
	SPI0_CTRLA |= SPI_ENABLE_bm;
}

uint8_t spi_tx(uint8_t byte) {
	SPI0_DATA = byte;

	while(!(SPI0_INTFLAGS & SPI_IF_bm));

	return SPI0_DATA;
}
