#include <avr/io.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>


void usart_init() {
	USART0_BAUD = (uint16_t)(1390);
	USART0_CTRLA = USART_TXCIE_bm;
	USART0_CTRLB = USART_TXEN_bm;
	USART0_CTRLC = USART_CMODE_ASYNCHRONOUS_gc | USART_PMODE_DISABLED_gc | USART_CHSIZE_8BIT_gc | USART_SBMODE_1BIT_gc;
}

void usart_send_char(uint8_t ch) {
	while (!(USART0_STATUS & USART_DREIF_bm));

	USART0_TXDATAL = ch;
}

void usart_send_string(char *str) {
	for(size_t i = 0; i < strlen(str); i++)
		usart_send_char(str[i]);
}

void usart_send_num(uint32_t num) {
	char buffer[8];

	if (num < 65536) {
		sprintf(buffer, "%ld\r\n", num);
		usart_send_string(buffer);
	}
}
