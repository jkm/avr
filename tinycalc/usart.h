void usart_init();
void usart_send_char(uint8_t ch);
void usart_send_string(char *str);
void usart_send_num(uint32_t num);
