#include <math.h>
#include <stdint.h>


uint8_t int2chr(uint32_t num, char *str) {
	if (num < 10) {
		str[0] = num + '0';
		str[1] = '\0';

		return 1;
	}

	int i, l, n, p;

	for (l = (int)log10(num), i = l; i >= 0; i--) {
		if (i == 0) {
			p = 1;
		} else if (i == 1) {
			p = 10;
		} else if (i == 2) {
			p = 100;
		} else if (i == 3) {
			p = 1000;
		} else if (i == 4) {
			p = 10000;
		}

		n = num / p;

		if (n == 0) {
			str[l - i] = '0';
		} else {
			num -= (n * p);
			str[l - i] = n + '0';
		}
	}

	str[l + 1] = '\0';

	return l + 1;
}
