#include <u8x8_avr.h>
#include <util/delay.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <avr/power.h>

#define DC_DDR  DDRB
#define DC_PORT PORTB
#define DC_BIT  0

#define RESET_DDR  DDRB
#define RESET_PORT PORTB
#define RESET_BIT  1

#define CS_DDR 	DDRB
#define CS_PORT PORTB
#define CS_BIT 	2

#define SWITCH_KEY_DELAY  50
#define SWITCH_MODE_DELAY 3000

u8g2_t u8g2;
char input[2][12];
char buffer[12];
char output[16];
int prec, currentMode, currentRow, status;
int isCalculated = 0;
double args[2];
double result;
const int rows[3] = { 16, 34, 64 };

char KEYS[5][4] = {
	{ 'M', 'R', 'D', 'C' },
	{ '7', '8', '9', '/' },
	{ '4', '5', '6', 'x' },
	{ '1', '2', '3', '-' },
	{ '0', '.', '=', '+' }
};

enum MODE {
	TERM_L_MODE,
	TERM_D_MODE,
	TERM_U_MODE,
	CALC_MODE,
	CALC_ADD_MODE,
	CALC_SUB_MODE,
	CALC_MUL_MODE,
	CALC_DIV_MODE
};

struct mode {
	enum MODE mode;
	char columns[4];
};

struct mode modes[8] = {
	{
		.mode = TERM_L_MODE,
		.columns = { 'U', 'd', 'L', '\0' }
	},
	{
		.mode = TERM_D_MODE,
		.columns = { 'L', 'U', 'd', '\0' }
	},
	{
		.mode = TERM_U_MODE,
		.columns = { 'L', 'd', 'U', '\0' }
	},
	{
		.mode = CALC_MODE,
		.columns = { ' ', ' ', '=', '\0' }
	},
	{
		.mode = CALC_ADD_MODE,
		.columns = { ' ', '+', '=', '\0' }
	},
	{
		.mode = CALC_SUB_MODE,
		.columns = { ' ', '-', '=', '\0' }
	},
	{
		.mode = CALC_MUL_MODE,
		.columns = { ' ', 'x', '=', '\0' }
	},
	{
		.mode = CALC_DIV_MODE,
		.columns = { ' ', '/', '=', '\0' }
	}
};

uint8_t getWholePartLength(double num) {
	char buffer[10];
	return strlen(itoa(num, buffer, 10));
}

uint8_t u8x8_gpio_and_delay(u8x8_t * u8x8, uint8_t msg, uint8_t arg_int, void *arg_ptr) {
	if (u8x8_avr_delay(u8x8, msg, arg_int, arg_ptr))
		return 1;

	switch (msg) {
		case U8X8_MSG_GPIO_AND_DELAY_INIT:
			CS_DDR |= _BV(CS_BIT);
			DC_DDR |= _BV(DC_BIT);
			RESET_DDR |= _BV(RESET_BIT);
			break;
		case U8X8_MSG_GPIO_CS:
			if (arg_int)
				CS_PORT |= _BV(CS_BIT);
			else
				CS_PORT &= ~_BV(CS_BIT);
			break;
		case U8X8_MSG_GPIO_DC:
			if (arg_int)
				DC_PORT |= _BV(DC_BIT);
			else
				DC_PORT &= ~_BV(DC_BIT);
			break;
		case U8X8_MSG_GPIO_RESET:
			if (arg_int)
				RESET_PORT |= _BV(RESET_BIT);
			else
				RESET_PORT &= ~_BV(RESET_BIT);
			break;
		default:
			u8x8_SetGPIOResult(u8x8, 1);
			break;
	}
	return 1;
}

void initDisplay() {
	u8g2_Setup_uc1701_ea_dogs102_f(
		&u8g2,
		U8G2_R0,
		u8x8_byte_avr_hw_spi,
		u8x8_gpio_and_delay
	);
	u8g2_InitDisplay(&u8g2);
	u8g2_SetPowerSave(&u8g2, 0);
}

char *getScientificNotation(double n) {
	char *sc = malloc(10 * sizeof(char));
	char buff[10];
	int p = floor(log10(fabs(n)));
	dtostrf(n * pow(10, -p), 4, 2, buff);
	sprintf(sc, "%se%d", buff, p);
	return sc;
}

void clearBuffer() {
	memset(buffer, 0, sizeof buffer);
}

void clearIsCalculated() {
	isCalculated = 0;
}

void setIsCalculated() {
	isCalculated = 1;
}

void setBuffer(int index, char *num) {
	if (currentMode == CALC_MODE && index == 1)
		strcat(buffer, " ");
	else if (currentMode > TERM_U_MODE && index == 2 && !isCalculated)
		strcat(buffer, " ");
	else
		sprintf(buffer, "%c", modes[currentMode].columns[index]);

	for (int i = 0; i < (9 - strlen(num)); i++)
		strcat(buffer, " ");

	strcat(buffer, num);
}

void renderLine(int index, char *num) {
	clearBuffer();
	setBuffer(index, num);

	u8g2_DrawStr(&u8g2, 0, rows[index], buffer);
}

void setInput(char key) {
	if (strchr(input[currentRow], '.') && key == '.')
		return;

	if (strlen(input[currentRow]) == 1 && input[currentRow][0] == '0' && key != '.')
		input[currentRow][0] = key;
	else
		input[currentRow][strlen(input[currentRow])] = key;

	input[currentRow][strlen(input[currentRow]) + 1] = '\0';
}

void deleteInput() {
	if (strlen(input[currentRow]) == 1) {
		memset(input[currentRow], 0, sizeof input[currentRow]);
		setInput('0');
	} else {
		input[currentRow][strlen(input[currentRow]) - 1] = '\0';
	}
}

void setRow(int row) {
	currentRow = row;
	if (!strlen(input[currentRow]))
		setInput('0');
}

void clearInput() {
	memset(input[0], 0, sizeof input[0]);
	memset(input[1], 0, sizeof input[1]);
	setRow(0);
}

void resetInput(int row) {
	memset(buffer, 0, sizeof buffer);
}

void clearOutput() {
	output[0] = '\0';
}

void trimOutput() {
	while (strchr(output, '.') && (output[strlen(output) - 1] == '0' || output[strlen(output) - 1] == '.'))
		output[strlen(output) - 1] = '\0';
}

void setArgs() {
	args[0] = atof(input[0]);
	args[1] = atof(input[1]);
}

void setMode(enum MODE mode) {
	currentMode = mode;
}

void setResult() {
	switch (currentMode) {
		case TERM_L_MODE:
			result = args[0] * args[1];
			break;
		case TERM_D_MODE:
			result = args[0] / args[1];
			break;
		case TERM_U_MODE:
			result = args[0] / args[1];
			break;
		case CALC_ADD_MODE:
			result = args[0] + args[1];
			break;
		case CALC_SUB_MODE:
			result = args[0] - args[1];
			break;
		case CALC_MUL_MODE:
			result = args[0] * args[1];
			break;
		case CALC_DIV_MODE:
			result = args[0] / args[1];
			break;
		default:
			break;
	}
}

void setOutput() {
	// Handle edge cases
	// When result == 0
	if (result == 0) {
		output[0] = '0';
		output[1] = '\0';
		return;
	}

	// When absolute value is less than 0.00001 or greater than 1000000
	if (fabs(result) < 0.00001 || fabs(result) > 1000000) {
		sprintf(output, getScientificNotation(result));
		return;
	}

	prec = 6 - getWholePartLength(result);
	dtostrf(result, 8, prec, output);
}

void calcMode() {
	if (!strlen(input[0]) || !strlen(input[1]))
		return;

	setArgs();
	setResult();
	setOutput();
	trimOutput();
}

void renderMode() {
	u8g2_ClearBuffer(&u8g2);
	u8g2_SetFont(&u8g2, u8g2_font_10x20_t_greek);

	renderLine(0, input[0]);
	renderLine(1, input[1]);
	renderLine(2, output);

	u8g2_SendBuffer(&u8g2);
}

void setInputPins() {
	DDRD &= ~(1 << 0);
	DDRD &= ~(1 << 1);
	DDRD &= ~(1 << 2);
	DDRD &= ~(1 << 3);
}

void setOutputPins() {
	DDRC |= (1 << 1) | (1 << 2) | (1 << 3) | (1 << 4) | (1 << 5);
}

char getKey() {
	for (int i = 1; i < 6; i++) {
		PORTC = (1 << i);

		_delay_ms(5);

		for (int j = 0; j < 4; j++) {
			status = (PIND & (1 << j)) >> j;

			if (status)
				return KEYS[5 - i][j];
		}
	}

	return ' ';
}

int isInCalcMode() {
	return currentMode > TERM_U_MODE;
}

void handleKey(char key) {
	switch (key) {
		case 'A':
			if (currentRow == 0)
				setRow(1);

			break;
		case 'C':
			clearInput();
			clearOutput();
			clearIsCalculated();
			setMode(currentMode > TERM_U_MODE ? CALC_MODE : currentMode);
			break;
		case 'D':
			deleteInput();
			break;
		case 'R':
			if (!isInCalcMode())
				setRow(1);
			break;
		case 'M':
			clearInput();
			clearOutput();
			clearIsCalculated();
			setMode(currentMode > TERM_U_MODE ? TERM_L_MODE : currentMode + 1);
			break;
		case '+':
			if (isInCalcMode() && !isCalculated) {
				setMode(CALC_ADD_MODE);
				setRow(1);
			}

			break;
		case '-':
			if (isInCalcMode() && !isCalculated) {
				setMode(CALC_SUB_MODE);
				setRow(1);
			}

			break;
		case 'x':
			if (isInCalcMode() && !isCalculated) {
				setMode(CALC_MUL_MODE);
				setRow(1);
			}

			break;
		case '/':
			if (isInCalcMode() && !isCalculated) {
				setMode(CALC_DIV_MODE);
				setRow(1);
			}

			break;
		case '=':
			if (currentRow == 1) {
				calcMode();
				setIsCalculated();
			}
			break;
		default:
			if (!isCalculated)
				setInput(key);
			break;
	}
}

int main() {
	initDisplay();

	setInputPins();
	setOutputPins();
	setMode(CALC_MODE);
	setRow(0);

	renderMode();

	while (1) {
		char key = getKey();
		if (key != ' ') {
			handleKey(key);
			renderMode();

			_delay_ms(SWITCH_KEY_DELAY);
		}
	}
}
